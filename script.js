/*Ответ на теоретический вопрос:

Потому что ввод может быть связан не только с событиями клавиатуры, а и с другими событиями (например голосовой ввод или копировать-вставить с помощью миши). Поэтому для обрабоки ввода лучше использовать событие input.
*/

const buttons = document.querySelectorAll('.btn');

document.addEventListener('keydown', function (event) {
    buttons.forEach(button => {
        button.classList.remove('active-btn');
        if (event.key === button.dataset.key || event.key.toLowerCase() === button.dataset.key) {
            button.classList.add('active-btn');
        }
    })
})